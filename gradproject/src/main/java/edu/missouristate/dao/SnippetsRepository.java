package edu.missouristate.dao;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import edu.missouristate.dao.custom.SnippetsRepositoryCustom;
import edu.missouristate.domain.Snippets;

public interface SnippetsRepository extends CrudRepository<Snippets, Integer>, SnippetsRepositoryCustom {

	String getViewDetails(Integer id);

	List<Snippets> getSnippetsByColumn(String column);

	List<Snippets> getSnippetsByColumn(String column, String searchText);
}
