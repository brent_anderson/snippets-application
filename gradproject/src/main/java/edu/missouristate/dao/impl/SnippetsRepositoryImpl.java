package edu.missouristate.dao.impl;

import java.sql.Timestamp;
import java.util.List;

import org.springframework.data.jpa.repository.support.QuerydslRepositorySupport;
import org.springframework.stereotype.Repository;

import edu.missouristate.dao.custom.SnippetsRepositoryCustom;
import edu.missouristate.domain.QSnippets;
import edu.missouristate.domain.Snippets;

@Repository
public class SnippetsRepositoryImpl extends QuerydslRepositorySupport implements SnippetsRepositoryCustom {

	QSnippets snippetsTable = QSnippets.snippets;

	public SnippetsRepositoryImpl() {
		super(Snippets.class);
	}

	public String getViewDetails(Integer id) {
		String snippet = getQuerydsl().createQuery()
				.from(snippetsTable)
				.select(snippetsTable.snippet)
				.where(snippetsTable.id.eq(id))
				.limit(1)
				.fetchOne();
		return snippet;
	}

	public List<Snippets> getSnippetsByColumn(String column) {
		@SuppressWarnings("unchecked")
		List<Snippets> list = (List<Snippets>)(Object)getQuerydsl().createQuery()
				.from(snippetsTable)
				.fetch();
		return list;
	}
	
	@SuppressWarnings("unchecked")
	public List<Snippets> getSnippetsByColumn(String column, String searchText) {
		
		List<Snippets> list = (List<Snippets>)(Object)getQuerydsl().createQuery()
				.from(snippetsTable)
				.fetch();
		if("author".equalsIgnoreCase(column)) {
			list = (List<Snippets>)(Object)getQuerydsl().createQuery()
					.from(snippetsTable)
					.where(snippetsTable.author.contains(searchText))
					.fetch();
			return list;
		}else if("snippetDate".equalsIgnoreCase(column)) {
			list = (List<Snippets>)(Object)getQuerydsl().createQuery()
					.from(snippetsTable)
					.where(snippetsTable.snippetDate.stringValue().substring(0, 10).contains(searchText))
					.fetch();
			return list;
		}else if("language".equalsIgnoreCase(column)) {
			list = (List<Snippets>)(Object)getQuerydsl().createQuery()
					.from(snippetsTable)
					.where(snippetsTable.language.contains(searchText))
					.fetch();
			return list;
		}else if("shortDescription".equalsIgnoreCase(column)) {
			list = (List<Snippets>)(Object)getQuerydsl().createQuery()
					.from(snippetsTable)
					.where(snippetsTable.shortDescription.contains(searchText))
					.fetch();
			return list;
		}else if("snippet".equalsIgnoreCase(column)) {
			list = (List<Snippets>)(Object)getQuerydsl().createQuery()
					.from(snippetsTable)
					.where(snippetsTable.snippet.contains(searchText))
					.fetch();
			return list;
		}
		
		return list;
	}

}
