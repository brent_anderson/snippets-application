package edu.missouristate.service;

import java.util.List;

import edu.missouristate.domain.Snippets;

public interface SnippetsService {

	List<Snippets> getSnippets();

	void saveSnippet(Snippets snippets);

	Snippets getSnippetById(Integer id);

	void deleteSnippet(Snippets snippet);

	String viewDetailsOfSnippet(Integer id);

	List<Snippets> getSnippetsByColumnSearch(String column);

	List<Snippets> getSnippetsByColumnSearch(String column, String searchText);
}
