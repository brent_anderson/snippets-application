package edu.missouristate.service.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import edu.missouristate.dao.SnippetsRepository;
import edu.missouristate.domain.Snippets;
import edu.missouristate.service.SnippetsService;

@Service("SnippetsService")
public class SnippetsServiceImpl implements SnippetsService{

	@Autowired
    SnippetsRepository snippetsRepository;
	
	
	@Override
	public List<Snippets> getSnippets() {
		List<Snippets> snippetList = (List<Snippets>) snippetsRepository.findAll();
		
		
		/*
		   There was an unexpected error (type=Internal Server Error, status=500).
		   Timestamp format must be yyyy-mm-dd hh:mm:ss[.fffffffff]
		   java.lang.IllegalArgumentException: Timestamp format must be yyyy-mm-dd hh:mm:ss[.fffffffff]
		   
		*/
//		for (Snippets snippet : snippetList) {
//			String now = snippet.getSnippetDate().toLocalDateTime().format(DateTimeFormatter.ofPattern("YYYY-MM-DD"));
//			Timestamp ts = Timestamp.valueOf(now);
//			snippet.setSnippetDate(ts);
//		}
		
		return snippetList;
	}
	
	@Override
	@Transactional
	public void saveSnippet(Snippets snippet) {
		snippetsRepository.save(snippet);
	}

	@Override
	public Snippets getSnippetById(Integer id) {
		Optional<Snippets> opt = snippetsRepository.findById(id);
		if(opt.isPresent()) {
			return opt.get();
		}
		else {
			return new Snippets();
		}
	}

	@Override
	@Transactional
	public void deleteSnippet(Snippets snippet) {
		snippetsRepository.delete(snippet);
		
	}
	
	
	@Override
	public String viewDetailsOfSnippet(Integer id) {
		return snippetsRepository.getViewDetails(id);
	}
	
	@Override
	public List<Snippets> getSnippetsByColumnSearch(String column){
		return snippetsRepository.getSnippetsByColumn(column);
	}

	@Override
	public List<Snippets> getSnippetsByColumnSearch(String column, String searchText) {
		return snippetsRepository.getSnippetsByColumn(column, searchText);
	}
	
	
}
