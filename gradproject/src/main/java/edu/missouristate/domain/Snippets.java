package edu.missouristate.domain;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.Generated;
import org.hibernate.annotations.GenerationTime;

@Entity
@Table(name = "snippets")
public class Snippets {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", columnDefinition = "INTEGER")
	private Integer id;
	
	@Column(name = "author", columnDefinition = "VARCHAR(32)")
	private String author;
	
	//@Column(name = "snippet_date", columnDefinition = "TIMESTAMP")
	//@CreationTimestamp
	@Generated(GenerationTime.ALWAYS)
	@Column(name = "snippet_date", columnDefinition = "TIMESTAMP DEFAULT CURRENT_TIMESTAMP")
	private Timestamp snippetDate;
	
	@Column(name = "language", columnDefinition = "VARCHAR(32)")
	private String language;
	
	@Column(name = "short_description", columnDefinition = "VARCHAR(128)")
	private String shortDescription;
	
	@Column(name = "snippet", columnDefinition = "VARCHAR(4096)")
	private String snippet;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getAuthor() {
		return author;
	}

	public void setAuthor(String author) {
		this.author = author;
	}

	public Timestamp getSnippetDate() {
		return snippetDate;
	}

	public void setSnippetDate(Timestamp snippetDate) {
		this.snippetDate = snippetDate;
	}

	public String getLanguage() {
		return language;
	}

	public void setLanguage(String language) {
		this.language = language;
	}

	public String getShortDescription() {
		return shortDescription;
	}

	public void setShortDescription(String shortDescription) {
		this.shortDescription = shortDescription;
	}

	public String getSnippet() {
		return snippet;
	}

	public void setSnippet(String snippet) {
		this.snippet = snippet;
	}
	
	public String getColumn(String column) {
		return getAuthor();
	}
}
