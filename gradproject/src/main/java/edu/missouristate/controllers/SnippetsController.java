package edu.missouristate.controllers;

import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

import edu.missouristate.domain.Snippets;
import edu.missouristate.service.SnippetsService;
import edu.missouristate.util.Helper;

@Controller
public class SnippetsController {

	@Autowired
	SnippetsService snippetsService;
	
	@GetMapping("/gradproject")
	public String getTable(HttpSession session, Model model) {
		List<Snippets> snippetList = snippetsService.getSnippets();
		
		
		//Helper.addSessionMessage(session, null);
		
		
		model.addAttribute("title", "Code Snippets");
		model.addAttribute("snippetList", snippetList);
		String sessionMessage = (String)session.getAttribute("message");
		
		if(sessionMessage == null || sessionMessage.length() == 0) {
			model.addAttribute("messageType", "success");
			model.addAttribute("messageBody", "Welcome!");
		}
		return "snippetsTable";
	}
	
	@GetMapping("/gradproject/getbycolumn")
	public String getSearch(Model model, String column, String searchText) {
		List<Snippets> snippetList = snippetsService.getSnippetsByColumnSearch(column, searchText);
		model.addAttribute("title", "Code Snippets");
		model.addAttribute("snippetList", snippetList);

		return "snippetsTable";
	}
	
	@GetMapping("/gradproject/addoredit")
	public String getAddEditTable(HttpSession session, Model model, Integer id) {
		
		if(id == null) {
			model.addAttribute("title", "Add Snippet");
		} else {
			model.addAttribute("title", "Edit Snippet");
			model.addAttribute("snippets", snippetsService.getSnippetById(id));
		}
		
		return "addEditSnippet";
	}
	
	@PostMapping("/gradproject/addoredit")
	public String postAddEditTable(Snippets snippet, BindingResult result, Model model, HttpSession session) {
		
		//snippetsService.saveSnippet(snippet);
		
		if(snippet.getId() != null) {
			snippetsService.saveSnippet(snippet);
			Helper.addSessionMessage(session, "Snippet updated successfully.");
		}else{
			snippetsService.saveSnippet(snippet);
			Helper.addSessionMessage(session, "Snippet added successfully.");
		}
		
		return "redirect:/gradproject";
	}
	
	@GetMapping("/gradproject/about")
	public String getAbout(Model model) {
		model.addAttribute("title", "About this Grad Project");
		return "about";
	}
	
	@GetMapping("/gradproject/delete")
	public String getDelete(HttpSession session, Model model, Integer id) {
		
		
		if(Helper.isInteger(id.toString())) {
			snippetsService.deleteSnippet(snippetsService.getSnippetById(id));
			Helper.addSessionMessage(session, "Record Deleted");
		}else {
			String idString = ((id == null) ? "''" : ("'" + id + "'"));
			Helper.addSessionMessage(session, "Unable to find id = " + idString);
		}
		
		
		//snippetsService.deleteSnippet(snippetsService.getSnippetById(id));
		return "redirect:/gradproject";
	}
		
	@GetMapping("/gradproject/viewdetails")
	public String getViewDetails(Model model, Integer id) {
		model.addAttribute("title", "View Details");
		String details = snippetsService.viewDetailsOfSnippet(id);
		model.addAttribute("details", details);
		return "viewDetails";
	}
}
