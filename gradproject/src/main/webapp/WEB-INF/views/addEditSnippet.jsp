<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<%@page import="java.util.Calendar"%>
<html lang="en">
<%@ include file="/WEB-INF/layouts/include.jsp"%>
<%@ include file="/WEB-INF/layouts/head.jsp"%>
<head>
<meta charset="ISO-8859-1">
<title>${title}</title>
<style>
html, body{
height: 100%;
width: 100%;
body: 100%;
}
.navbar {
	background-color: #D0FAFB !important;
}

#home:hover, #catagories:hover, #aboutUs:hover {
	background-color: #000099;
	color: #999;
}
body {
	background-color: #D4FEFF;
}

#div1 {
	background-color: #E0FEFF;
}

label {
	margin-top: 10px;
}

#centerMeDiv {
	margin: 5% 0 0 35%;
}

#submitBtn {
	width: 150px;
	font-size: 125%;
	border-color: rgb(0, 157, 87);
}

#submitBtn:hover {
	background-color: #000099;
	width: 150px;
	font-size: 125%;
	border-color: rgb(0, 157, 87);
}

textarea {
	overflow: auto;
 	display: block; 
}
#snippet{
	<!-- height: 15em; -->
}
#footer {
	position: sticky;
	top: 2000px;
	bottom: 0;
	left: 0;
	text-align: center;
	height: 50px;
	line-height: 50px;
	vertical-align: middle;
	background-color: #D4FEFF;
}
</style>
</head>
<body>
	<nav class="navbar navbar-expand-lg navbar-light bg-light">
		<a class="navbar-brand" href="javascript:void(0);">Snippets R So Cool Bro</a>
		<button class="navbar-toggler" type="button" data-toggle="collapse"
			data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup"
			aria-expanded="false" aria-label="Toggle navigation">
			<span class="navbar-toggler-icon"></span>
		</button>
		<div class="collapse navbar-collapse" id="navbarNavAltMarkup">
			<div class="navbar-nav">
				<a id="home" class="nav-item nav-link active"
					href="<%=request.getContextPath()%>/gradproject">Home<span
					class="sr-only">(current)</span></a>
				<a id="aboutUs" class="nav-item nav-link"
					href="<%=request.getContextPath()%>/gradproject/about">About</a>
			</div>
		</div>
	</nav>
	<div id="div1" class="container">
		<div class="row">
			<div class="col-sm-12">
			<h2>${title}</h2>
			<form action="<%=request.getContextPath()%>/gradproject/addoredit" method="post" >
			
				<input type="hidden" name="id" value="${snippets.id}"/>
				<div class="form-group">
					<label for="author">author</label>
					<input type="text" class="form-control" name="author" value="${snippets.author}">
				</div>
					<input type="hidden" class="form-control" name="snippetDate" id="snippetDate" value="${snippets.snippetDate}">
				<div class="form-group">
					<label for="language">language</label>
					<input type="text" class="form-control" name="language" value="${snippets.language}">
				</div>
				<div class="form-group">
					<label for="shortDescription">shortDescription</label>
					<input type="text" class="form-control" name="shortDescription" value="${snippets.shortDescription}">
				</div>
				<div class="form-group">
					<label for="snippet">snippet</label>
					<input type="text" class="form-control" name="snippet" id="snippet" value="${snippets.snippet}">
				</div>
			<button name="submitBtn" class="btn btn-primary">Submit</button>
			</form>
			</div>
		</div>
	</div>
	<div id="footer" class="container-fluid border">Brent Anderson
		&copy;<%
				out.print(Calendar.getInstance().get(Calendar.YEAR));
			%>
	</div>
</body>

<script>
</script>
</html>