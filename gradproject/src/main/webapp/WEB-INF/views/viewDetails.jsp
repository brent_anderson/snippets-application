<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<%@page import="java.util.Calendar"%>
<html lang="en">
<%@ include file="/WEB-INF/layouts/include.jsp"%>
<%@ include file="/WEB-INF/layouts/head.jsp"%>
<head>
<meta charset="ISO-8859-1">
<title>${title}</title>
<style>
.navbar {
	background-color: #D0FAFB !important;
}

#home:hover, #catagories:hover, #aboutUs:hover {
	background-color: #000099;
	color: #999;
}

body {
	background-color: #D4FEFF;
}

#div1 {
	background-color: #E0FEFF;
}

label {
	margin-top: 10px;
}

#centerMeDiv {
	margin: 5% 0 0 35%;
}

#submitBtn {
	width: 150px;
	font-size: 125%;
	border-color: rgb(0, 157, 87);
}

#submitBtn:hover {
	background-color: #000099;
	width: 150px;
	font-size: 125%;
	border-color: rgb(0, 157, 87);
}

textarea {
	width: 75%;
	height: 25%;
	overflow: auto;
	margin-top: 5%;
}
#taDiv{
	width: 100%;
	height: 90%;
	text-align: center;
	
}
#snippet {
	height: 15em;
}

#footer {
	position: sticky;
	top: 2000px;
	bottom: 0;
	left: 0;
	text-align: center;
	height: 50px;
	line-height: 50px;
	vertical-align: middle;
	background-color: #D4FEFF;
}

html, body {
	height: 100%;
	width: 100%;
	body: 100%;
}
</style>
</head>
<body>
	<nav class="navbar navbar-expand-lg navbar-light bg-light">
		<a class="navbar-brand" href="#">Snippets R So Cool Bro</a>
		<button class="navbar-toggler" type="button" data-toggle="collapse"
			data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup"
			aria-expanded="false" aria-label="Toggle navigation">
			<span class="navbar-toggler-icon"></span>
		</button>
		<div class="collapse navbar-collapse" id="navbarNavAltMarkup">
			<div class="navbar-nav">
				<a id="home" class="nav-item nav-link active"
					href="<%=request.getContextPath()%>/gradproject">Home<span
					class="sr-only">(current)</span></a> <a id="aboutUs"
					class="nav-item nav-link"
					href="<%=request.getContextPath()%>/gradproject/about">About</a>
			</div>
		</div>
	</nav>
	
	
	<div id="taDiv">
		<textarea readonly>${details}</textarea>
	</div>


	<div id="footer" class="container-fluid border">
		Brent Anderson &copy;<%
		out.print(Calendar.getInstance().get(Calendar.YEAR));
	%>
	</div>
</body>
</html>