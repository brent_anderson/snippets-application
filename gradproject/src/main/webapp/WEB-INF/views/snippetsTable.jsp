<!doctype>
<%@page import="java.util.Calendar"%>
<html lang="en">
<%@ include file="/WEB-INF/layouts/include.jsp"%>
<%@ include file="/WEB-INF/layouts/head.jsp"%>
<head>
<title>Code Snippets</title>

<!-- Meta Tags -->
<meta name="viewport"
	content="width=device-width, initial-scale=1, shrink-to-fit=no">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />

<!-- JQuery/Popper JS -->
<script src="https://code.jquery.com/jquery-3.4.1.slim.min.js"
	integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n"
	crossorigin="anonymous"></script>
<script
	src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"
	integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo"
	crossorigin="anonymous"></script>

<!-- Boostrap 4 CSS -->
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css"
	integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh"
	crossorigin="anonymous">
<script
	src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"
	integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6"
	crossorigin="anonymous"></script>

<style>
#addPartButton:hover {
	vertical-align: middle;
	background-color: #000099;
}

#home:hover, #catagories:hover, #aboutUs:hover {
	background-color: #000099;
	color: #999;
}

#div1 {
	background-color: #E0FEFF;
}

.navbar {
	background-color: #D0FAFB !important;
}

body {
	background-color: #D4FEFF;
}

h1 {
	font-style: italic;
	font-size: 5em;
	color: rgb(0, 87, 157);
	text-shadow: -1px -1px #000;
	text-align: center;
	vertical-align: middle;
}

#tableHeader {
	font-style: italic;
	font-size: 25px;
}

#tbody {
	font-style: oblique;
	font-size: 15px;
	margin-bottom: -50px;
}

#footer {
	position: sticky;
	top: 2000px;
	bottom: 0;
	left: 0;
	text-align: center;
	height: 50px;
	line-height: 50px;
	vertical-align: middle;
	background-color: #D4FEFF;
}

#deleteSnippet {
	/* color: #FF0000; */
}

#addEditSnippets {
	color: #ffb605;
}

#message {
	width: 300px;
	vertical-align: middle;
	margin-left: 10px;
}

#searchH3 {
	display: inline;
}
</style>
</head>
<body>
	<nav class="navbar navbar-expand-lg navbar-light bg-light">
		<a class="navbar-brand" href="javascript:void(0);">Snippets R So Cool Bro</a>
		<button class="navbar-toggler" type="button" data-toggle="collapse"
			data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup"
			aria-expanded="false" aria-label="Toggle navigation">
			<span class="navbar-toggler-icon"></span>
		</button>
		<div class="collapse navbar-collapse" id="navbarNavAltMarkup">
			<div class="navbar-nav">
				<a id="home" class="nav-item nav-link active"
					href="<%=request.getContextPath()%>/gradproject">Home<span
					class="sr-only">(current)</span></a> <a id="aboutUs"
					class="nav-item nav-link"
					href="<%=request.getContextPath()%>/gradproject/about">About</a>
			</div>
		</div>
	</nav>
	<div id="div1" class="container">
		<h1>${title}</h1>
		<div class="row">
			<div class="col-sm-6">
				<a id="addPartButton" class="btn btn-primary btn-lg btn-center mb-2"
					href="<%=request.getContextPath()%>/gradproject/addoredit">Add Snippet</a>
			</div>
			<div class="col-sm-6">
				<h3 id="searchH3">Search:</h3>
				<select name="snippets" id="snippets">
					<option value="author">Author</option>
					<option value="snippetDate">Date</option>
					<option value="language">Language</option>
					<option value="shortDescription">Short Description</option>
					<option value="snippet">Snippet</option>
				</select> <input id="searchInput" placeholder="Search Text"></input> 
				
				<!-- The href in the anchor below (id="goButton") is worthless, it's being replaced in the script at the end of this page. -->
				<a id="goButton"
					class="btn btn-primary btn-lg btn-center mb-2"
					href="<%=request.getContextPath()%>/gradproject/getbycolumn?column=">Go</a>
			</div>
		</div>

		<div></div>
		<%@ include file="/WEB-INF/layouts/message.jsp"%>
		
		
		
		
		<table id="myTable" class="table table-striped table-bordered table-hover mt-3">
			<tr id="tableHeader">
				<th>Action</th>
				<th>Author</th>
				<th>Date</th>
				<th>Language</th>
				<th>Short Description</th>
				<th>Snippet</th>
			</tr>
			<tbody id="tbody">
				<c:forEach var="snippet" items="${snippetList}">
					<tr>
						<td>
						
						<a class="no-decor" id="addEditSnippets"
							href="<%=request.getContextPath()%>/gradproject/addoredit?id=${snippet.id}">
								<i id="edit_${snippet.id}" class="fas fa-pencil-alt" aria-hidden="true"></i>
						</a> 
						<a id="d_${snippet.id}" type="button"  data-toggle="modal" data-target="#exampleModal">
								<i id="delete_${snippet.id}" class="fa fa-times ml-2" aria-hidden="true"></i>
						</a>
						
						</td>
						<td>${snippet.author}</td>
						<td><fmt:formatDate pattern="yyyy-MM-dd" value="${snippet.snippetDate}" /></td>
						<td>${snippet.language}</td>
						<td>${snippet.shortDescription}</td>
						<td>
						<a class="no-decor" id="viewDetails" href="<%=request.getContextPath()%>/gradproject/viewdetails?id=${snippet.id}">View Details</a>
						</td>
					</tr>
				</c:forEach>
			</tbody>
		</table>
	</div>
	
	
	<!-- Delete Modal -->
	<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	  <div class="modal-dialog" role="document">
	    <div class="modal-content">
	      <div class="modal-header">
	        <h5 class="modal-title" id="exampleModalLabel">Are you sure you want to delete this snippet?</h5>
	      </div>
	      <div class="modal-footer">
	       <a class="btn btn-primary no-decor" id="deleteSnippet" href="<%=request.getContextPath()%>/gradproject/delete?id=${snippet.id}">Yes</a>
	        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
	      </div>
	    </div>
	  </div>
	</div>
	
	
	<div id="footer" class="container-fluid border">Brent Anderson
		&copy;<%
				out.print(Calendar.getInstance().get(Calendar.YEAR));
			%>
	</div>
	
	<script>
	
	//Declare Global Variables
	var parentOfPath, searchInput, myTable;
	
	
	window.addEventListener("DOMContentLoaded", function(){
		displayCorrectDateFormat();
		search();
		addEventListeners();
	});
	
	function displayCorrectDateFormat(){
		
	}
	
	function addEventListeners(){
		deleteByIdListener();
	}
	
	function deleteByIdListener(){
		document.getElementById("myTable").addEventListener("click", (e)=>{
			parentOfPath = 'a';
			if(e.target.tagName == "path"){
				parentOfPath = e.target.parentElement.id;
			}
			if(e.target.id.substring(0, 1) == 'd'){
				let str = e.target.id.replace(/[^0-9]/g, "");
				document.getElementById("deleteSnippet").setAttribute("href","<%=request.getContextPath()%>/gradproject/delete?id="+str);	
			}else if(parentOfPath.substring(0, 1) == 'd'){
				let str = parentOfPath.replace(/[^0-9]/g, "");
				document.getElementById("deleteSnippet").setAttribute("href","<%=request.getContextPath()%>/gradproject/delete?id="+str);	
			}
		});
	}
	
	
	//This function gets the value for the selected column seach option as selectedValue (default is Author)
	function search(){
		
		var selectedValue = document.getElementById("snippets").value;
		document.getElementById("goButton").setAttribute("href","<%=request.getContextPath()%>/gradproject/getbycolumn?column="+selectedValue+"&searchText="+"");
		
		document.getElementById("snippets").addEventListener('change', (e)=>{
			selectedValue = document.getElementById("snippets").value;
			searchInput = document.getElementById("searchInput").value;
			document.getElementById("goButton").setAttribute("href","<%=request.getContextPath()%>/gradproject/getbycolumn?column="+selectedValue+"&searchText="+searchInput);
		});
		
		document.getElementById("searchInput").addEventListener('change', (e)=>{
			selectedValue = document.getElementById("snippets").value;
			searchInput = document.getElementById("searchInput").value;
			document.getElementById("goButton").setAttribute("href","<%=request.getContextPath()%>/gradproject/getbycolumn?column="+selectedValue+"&searchText="+searchInput);
		});
	}
	
	
	<%-- function search(){
		var selectElem = document.getElementById("snippets");
		var selectedValue = document.getElementById("snippets").value;
		
		
		
		searchInput = document.getElementById("searchInput").value;
		
		document.getElementById("goButton").setAttribute("href","<%=request.getContextPath()%>/gradproject/getbycolumn?column="+selectedValue+"&searchText="+searchInput);
		selectElem.addEventListener('change', (e)=>{
			var selectedValue = document.getElementById("snippets").value;
			searchInput = document.getElementById("searchInput").value;
			document.getElementById("goButton").setAttribute("href","<%=request.getContextPath()%>/gradproject/getbycolumn?column="+selectedValue+"&searchText="+searchInput);
		});
	} --%>
	</script>
</body>
</html>