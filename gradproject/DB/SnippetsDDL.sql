CREATE DATABASE `snippets` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci */ /*!80016 DEFAULT ENCRYPTION='N' */;

CREATE TABLE IF NOT EXISTS snippets(
id INTEGER NOT NULL auto_increment,
author varchar(32) NOT NULL,
snippet_date timestamp NOT NULL default CURRENT_TIMESTAMP,
language VARCHAR(32) NOT NULL,
short_description VARCHAR(128) NOT NULL,
snippet VARCHAR(4096) NOT NULL,
PRIMARY KEY (id)
);

--Feel free to use the code below to prime the table if you'd like.
/*
INSERT INTO snippets (id, author, language, short_description, snippet)
VALUES 
(null, 'asdf5',  'alkj1', 'desc1', 'foreach1'),
(null, 'asdf4', 'alkj2', 'desc2', 'foreach2'),
(null, 'asdf3', 'alkj3', 'desc3', 'foreach3'),
(null, 'asdf2', 'alkj4', 'desc4', 'foreach4'),
(null, 'asdf1', 'alkj5', 'desc5', 'foreach5');
*/